<?php

namespace App\Exceptions;

use Exception;
use App\Events\UncaughtQueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        // if we are not in debug mode...
        if (!config('app.debug')) {
            // render model-not-found and method-not-allowed exceptions as 404
            if ($exception instanceof ModelNotFoundException || $exception instanceof MethodNotAllowedHttpException) {
                return parent::render($request, new NotFoundHttpException());
            } else if (!$this->isHttpException($exception)) {
                // render the exception as 500
                return response(null, 500)->view('errors.500');
            }
        }

        if ($exception instanceof QueryException) {
            event(new UncaughtQueryException($exception));
        }

        // we are in debug mode; return the raw exception and display all details
        return parent::render($request, $exception);
    }
}
