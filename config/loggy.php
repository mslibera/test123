<?php

return [

    'fire_event' => true, // set to false if Loggy should not fire LoggyMessageLogged event upon writing to logs

    'channels' => [
        'general' => [
            'log' => 'general.log',
            'daily' => true,
            'level' => 'debug'
        ],

        'queue' => [
            'log' => 'queue.log',
            'daily' => true,
            'level' => 'debug'
        ],

        'cron' => [
            'log' => 'cron.log',
            'daily' => true,
            'level' => 'debug'
        ],

        'access' => [
            'log' => 'access.log',
            'daily' => true,
            'level' => 'debug'
        ],

        'database' => [
            'log' => 'database.log',
            'daily' => true,
            'level' => 'debug'
        ],

        'acl' => [
            'log' => 'acl.log',
            'daily' => true,
            'level' => 'debug'
        ],

        'backup' => [
            'log' => 'backup.log',
            'daily' => true,
            'level' => 'debug'
        ],
    ]
];