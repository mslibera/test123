<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application variables
    |--------------------------------------------------------------------------
    |
    */

    'search' => env('APP_SEARCH', false), // should we show the search?

    'login_methods' => explode(',', env('APP_LOGIN_METHODS', 'local')),

    'allow_signups' => env('APP_ALLOW_SIGNUPS', 'false') == 'true',

    'paginator_per_page' => env('PAGINATOR_PER_PAGE', 25),

    'queue_failures_allowed' => [
        'default' => env('APP_QUEUE_FAILURES_ALLOWED_DEFAULT', 0),
        'email'   => env('APP_QUEUE_FAILURES_ALLOWED_EMAIL', 3),
        'api'     => env('APP_QUEUE_FAILURES_ALLOWED_API', 5),
    ],

    /*
    |--------------------------------------------------------------------------
    | CCPS Modules
    |--------------------------------------------------------------------------
    |
    | Configuration for CCPS modules. Core CCPS modules are set up upon
    | installation of 'ccps-core'. Others will need to be added as packages are
    | added.
    |
    | Default configuration can be changed.
    | Icons and menu items will be displayed in the order in which they appear in the array below.
    |
    |
    */

    'modules' => [

        'users' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'users',
            'title' => 'Users',
            'index' => 'users',
            'parent' => 'admin',
            'required_permissions' => 'users.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'acl' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'list',
            'title' => 'ACL',
            'index' => 'acl',
            'parent' => 'admin',
            'required_permissions' => 'acl.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'email' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'envelope',
            'title' => 'App Email',
            'index' => 'email',
            'parent' => 'admin',
            'required_permissions' => 'email.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'queues' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'server',
            'title' => 'Queues &amp; Jobs',
            'index' => 'queues',
            'parent' => 'admin',
            'required_permissions' => 'queues.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'cronjobs' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'hourglass-half',
            'title' => 'Cron Jobs',
            'index' => 'cronjobs',
            'parent' => 'admin',
            'required_permissions' => 'cronjobs.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'log-viewer' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'table',
            'title' => 'Log Viewer',
            'index' => 'log-viewer',
            'parent' => 'admin',
            'required_permissions' => 'logs.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'cache' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'database',
            'title' => 'Cache',
            'index' => 'cache',
            'parent' => 'admin',
            'required_permissions' => 'cache.clear',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

        'config' => [
            'package' => 'uncgits/ccps-core',
            'icon' => 'cog',
            'title' => 'Config',
            'index' => 'config',
            'parent' => 'admin',
            'required_permissions' => 'config.*',
            'use_custom_routes' => false,
            'custom_view_path' => false,
        ],

    ],

];
