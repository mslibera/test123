<a href="{{ $url }}">
    <div class="col-md-{{ $span or "2" }}">
        <div class="panel panel-{{ $class or "default" }}">
            <div class="panel-body text-center">
                <i class="fa fa-5x fa-{{ $fa }}"></i>
            </div>
            <div class="panel-footer text-center">
                {{ $title }}
            </div>
        </div>
    </div>
</a>