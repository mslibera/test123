@extends('layouts.wrapper', [
    'pageTitle' => 'My Account'
])

@section('content')
    {!! Breadcrumbs::render('account') !!}
    <h1>My Account</h1>

    <p><a class="btn btn-danger" href="{{ route('logout') }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <i class="fa fa-power-off"></i> Log Out
        </a></p>

    <hr>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

    <div class="row">
        @include('components.panel-nav', [
            'url' => route('profile.show'),
            'fa' => 'user',
            'title' => 'My Profile'
        ])
        @include('components.panel-nav', [
            'url' => route('account.settings'),
            'fa' => 'cog',
            'title' => 'Account Settings'
        ])
    </div>
@endsection