@extends('layouts.wrapper', [
    'pageTitle' => '404'
])

@section('content')
    <h2>404 - Not Found</h2>
    <p>{{ $exception->getMessage() }}</p>
@endsection()