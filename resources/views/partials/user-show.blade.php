<h1>User - {{ $userToShow->email }}</h1>

<div class="row">
    <div class="col-md-2">
        <img class="img-circle img-responsive" src="{{ $userToShow->avatar_url or asset('images/default-avatar.jpg')}}">
    </div>
    <div class="col-md-10">
        <p>Name: {{ $userToShow->first_name }} {{ $userToShow->last_name }}</p>
        <p>Provider: {{ $userToShow->provider }}</p>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-12">
        <h3>Actions</h3>
        @if($context == 'profile')
            <a href="{{ route('profile.edit') }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
        @else
            @permission('users.edit')
            <a href="{{ route('user.edit', ['userToEdit' => $userToShow->id]) }}" class="btn btn-warning"><i class="fa fa-pencil"></i> Edit</a>
            @endpermission
            @permission('users.delete')
                <form method="POST" action="{{ route('user.destroy', ['user' => $userToShow->id]) }}"
                      onsubmit="return confirm('Do you really want to delete this user?')">

                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>

                    {{ csrf_field() }}
                    {{ method_field("DELETE") }}
                </form>
            @endpermission
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>User Roles</h2>
        @if(count($userToShow->roles) > 0)
            <ul>
                @foreach($userToShow->roles as $index => $role)
                    <li>{{ $role->display_name or $role->name }}</li>
                @endforeach
            </ul>
        @else
            <p>No roles are currently assigned to this user.</p>
        @endif

        <h2>Permissions for this user</h2>
        @if(count($userToShow->permissions) > 0)
            <ul>
                @foreach($userToShow->permissions as $index => $permission)
                    <li>{{ $permission->display_name }}</li>
                @endforeach
            </ul>
        @else
            <p>No permissions are currently assigned to this user.</p>
        @endif
    </div>
</div>