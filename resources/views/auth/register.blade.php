@extends('layouts.wrapper', [
    'pageTitle' => "Register"
])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    @foreach($appLoginMethods as $index => $method)
                        <li role="presentation" class="{{ $index == 0 ? 'active' : '' }}"><a href="#{{ $method }}" aria-controls="{{ $method }}" role="tab" data-toggle="tab">{{ ucwords($method) }}</a></li>
                    @endforeach
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    @foreach($appLoginMethods as $index => $method)
                        <div role="tabpanel" class="tab-pane {{ $index == 0 ? 'active' : '' }}" id="{{ $method }}">
                            @include('auth.partials.register.' . $method)
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
    </div>
@endsection