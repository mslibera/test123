<div class="panel panel-default">
    <div class="panel-heading">Google</div>
    <div class="panel-body text-center">
        <a href="{{ route('oauth', ['provider' => 'google']) }}" class="btn btn-primary btn-success" title="Login with Google">
            <i class="fa fa-2x fa-google-plus-official"></i> Log in with Google
        </a>
    </div>
</div>