<div class="panel panel-default">
    <div class="panel-heading">Google</div>
    <div class="panel-body text-center">
        <a href="{{ route('oauth', ['provider' => 'google']) }}" class="btn btn-primary btn-success" title="Create account via Google">
            <i class="fa fa-2x fa-google-plus-official"></i> Create account via Google
        </a>
    </div>
</div>